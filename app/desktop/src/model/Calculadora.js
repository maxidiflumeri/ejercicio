Ext.define('MyExtGenApp.model.Calculadora', {
    extend: 'MyExtGenApp.model.Base',
    fields: [
        'operacion', 'primerValor', 'segundoValor', 'resultado'
    ]
});