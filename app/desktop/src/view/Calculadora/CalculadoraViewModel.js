Ext.define('MyExtGenApp.view.calculadora.CalculadoraViewModel', {
	extend: 'Ext.app.ViewModel',
	alias: 'viewmodel.calculadoraviewmodel',
	data: {
		name: 'MyExtGenApp'
	},
	stores: {
		calcStore: {
			type:'calculadoraviewstore',			
			autoLoad: true
		}
	}
});
