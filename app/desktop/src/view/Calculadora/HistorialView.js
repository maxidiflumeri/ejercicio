Ext.define('MyExtGenApp.view.Calculadora.HistorialView', {
    extend: 'Ext.grid.Grid',
    xtype: 'historial',    
    id: 'historial',
    requires: ['Ext.grid.rowedit.Plugin'],    
    viewModel: { type: 'calculadoraviewmodel' },
    bind: '{calcStore}',            
    columns: [
        {
            text: 'Operacion',
            dataIndex: 'operacion'
        },
        {
            text: 'Primer Valor',
            dataIndex: 'primerValor'
        },
        {
            text: 'Segundo Valor',
            dataIndex: 'segundoValor'
        },
        {
            text: 'Resultado',
            dataIndex: 'resultado'
        }
    ],
    items: [
        {
            text: 'Reset',
            xtype: 'button',
            width: 250,
            handler: function () {
                var store = Ext.getStore('calculadoraviewstore');
                store.loadData([], false)                
                console.log(store)
            }
        }
    ]
});