Ext.define('MyExtGenApp.view.calculadora.calculadoraView', {
    extend: 'Ext.tab.Panel',
    xtype: 'calculadoraview',
    id: 'calculadoraview',
    cls: 'calculadoraview',
    require: ['MyExtGenApp.view.Calculadora.HistorialView'],
    controller: { type: 'calculadoraviewcontroller' },
    viewModel: { type: 'calculadoraviewmodel' },
    grouped: true,
    groupFooter: {
        xtype: 'gridsummaryrow'
    },
    width: 500,
    height: 300,
    defaults: {
        bodyPadding: 10,
        scrollable: true
    },

    items: [
        {
            title: 'Cálculos',            
            align: 'center',
            pack: 'center',
            items:
                [
                    {
                        label: 'Valor 1',
                        name: 'valor1',
                        id: 'valor1',
                        xtype: 'numberfield',
                        style: 'margin: 50px 50px 50px 50px',
                        width: 250
                    },
                    {
                        label: 'Valor 2',
                        name: 'valor2',
                        id: 'valor2',
                        xtype: 'numberfield',
                        style: 'margin: 50px 50px 50px 50px',
                        width: 250
                    },
                    {
                        xtype: 'combobox',
                        label: 'Operación',
                        valueField: 'id',
                        displayField: 'operacion',
                        id: 'operacion',
                        store: [
                            { id: 1, operacion: 'suma' },
                            { id: 2, operacion: 'resta' },
                            { id: 3, operacion: 'division' },
                            { id: 4, operacion: 'multiplicacion' }
                        ],
                        style: 'margin: 50px 50px 50px 50px',
                        width: 250
                    },
                    {
                        label: 'Resultado',
                        name: 'resultado',
                        id: 'resultado',
                        xtype: 'numberfield',
                        style: 'margin: 50px 50px 50px 50px',
                        width: 250,
                        readOnly: true
                    },
                    {
                        viewModel: { type: 'calculadoraviewmodel' },
                        text: 'calcular',
                        iconCls: "x-fa fa-calculator",
                        xtype: 'button',
                        style: 'margin: 50px 50px 50px 50px',
                        width: 250,
                        handler: function () {
                            var resultado = 0
                            var _valor1 = Ext.getCmp('valor1').getValue()
                            var _valor2 = Ext.getCmp('valor2').getValue()
                            var _operacion = Ext.getCmp('operacion').getValue()
                            switch (_operacion) {
                                case 1:
                                    resultado = _valor1 + _valor2
                                    break
                                case 2:
                                    resultado = _valor1 - _valor2
                                    break
                                case 3:
                                    resultado = _valor1 / _valor2
                                    break
                                case 4:
                                    resultado = _valor1 * _valor2
                                    break

                            }

                            Ext.getCmp('resultado').setValue(resultado)                            
                            var store = Ext.getStore('calculadoraviewstore');
                            var objeto = { operacion: _operacion, primerValor: _valor1, segundoValor: _valor2, resultado: resultado }
                            store.insert(0, [objeto]);
                            store.sync();
                            console.log('store: ', store);                            
                            Ext.getCmp('valor1').setValue('')
                            Ext.getCmp('valor2').setValue('')
                            Ext.getCmp('operacion').setValue('')
                        }
                    },
                ],
        },
        {
            title:'Historial',                                    
            bind: '{calcStore}',            
            requires: ['Ext.grid.rowedit.Plugin'],            
            xtype: 'grid',
            columns: [
                {
                    text: 'Operacion',
                    dataIndex: 'operacion',                    
                },
                {
                    text: 'Primer Valor',
                    dataIndex: 'primerValor'
                },
                {
                    text: 'Segundo Valor',
                    dataIndex: 'segundoValor'
                },
                {
                    text: 'Resultado',
                    dataIndex: 'resultado'
                }
            ],
            items:[                
                {
                    text: 'Reset',
                    xtype: 'button',
                    width: 250,
                    handler: function () {
                        var store = Ext.getStore('calculadoraviewstore');                        
                        store.loadData([], false)   
                        console.log(Ext.getCmp('calculadoraview'))
                        console.log(store)                                                                                                                        
                    }
                }    
            ]           
        }
    ]
});















