Ext.define('MyExtGenApp.view.Calculadora.CalculadoraViewStore', {
    extend: 'Ext.data.Store',
    alias: 'store.calculadoraviewstore',
    storeId: 'calculadoraviewstore',
    model: 'MyExtGenApp.model.Calculadora',
    //  fields: [
    //      'operacion', 'primerValor', 'segundoValor', 'resultado'
    //  ],
    data: {
        items: [
            { operacion: 1, primerValor: 10, segundoValor: 15, resultado: 25 },
            { operacion: 1, primerValor: 10, segundoValor: 17, resultado: 27 }
        ]
    },
    proxy: {
        type: 'memory',
        reader: {
            type: 'json',
            rootProperty: 'items'
        }
    }
});