Ext.define('MyExtGenApp.view.personnel.PersonnelView',{
    extend: 'Ext.grid.Grid',
    xtype: 'personnelview',
    cls: 'personnelview',
    requires: ['Ext.grid.rowedit.Plugin', 'MyExtGenApp.view.Calculadora.CalculadoraViewStore'],
    controller: {type: 'personnelviewcontroller'},
    viewModel: {type: 'personnelviewmodel'},
    // store: {type: 'personnelviewstore'},
    store: {type: 'calculadoraviewstore'},
    grouped: true,
    groupFooter: {
        xtype: 'gridsummaryrow'
    },
    plugins: {
        rowedit: {
            autoConfirm: false
        }
    },
    
    columns: [
        {
            text: 'Operacion',
            dataIndex: 'operacion',
            editable: false,
            width: 100,
            cell: {userCls: 'bold'}
        },
        {text: 'Primer Valor',dataIndex: 'primerValor',editable: false, width: 230},
        {
            text: 'Segundo Valor',
            dataIndex: 'segundoValor',
            editable: false,
            width: 150
        },
        {
            text: 'Resultado',
            dataIndex: 'resultado',
            editable: false,
            width: 150
        },
    ],

    items: [
        {
            text: 'Reset',
            xtype: 'button',
            width: 250,
            handler: function () {
                var store = Ext.getStore('calculadoraviewstore');
                // store.loadData([], false)                
                console.log(store)
            }
        }
    ],
    
    listeners: {
        canceledit: 'onEditCancelled'
    }
});






